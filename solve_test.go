package main

import (
	"context"
	"sync"
	"testing"
)

func TestSolverStep_moveOptions(t *testing.T) {
	type fields struct {
		Rack   Rack
		tries  *tries
		Parent *SolverStep
	}
	type args struct {
		ch chan SolverStep
	}

	testRack := Rack{
		Tube{content: [4]waterColour{1, 2, 3, 4}},
		Tube{content: [4]waterColour{2, 2, 3, 4}},
		Tube{content: [4]waterColour{1, 2, 3, 4}},
		Tube{content: [4]waterColour{0, 0, 2, 4}},
		Tube{content: [4]waterColour{0, 2, 3, 4}},
		Tube{content: [4]waterColour{0, 0, 0, 0}},
		Tube{content: [4]waterColour{0, 0, 0, 0}},
	}

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "fixedRack",
			fields: fields{
				Rack: testRack,
				tries: &tries{
					Mutex:  &sync.Mutex{},
					states: map[rackSum]tryState{},
				},
				Parent: nil,
			},
			args: args{make(chan SolverStep, 33)},
		},
		{
			name: "generatedRack",
			fields: fields{
				Rack: GenerateRack(3),
				tries: &tries{
					Mutex:  &sync.Mutex{},
					states: map[rackSum]tryState{},
				},
				Parent: nil,
			},
			args: args{make(chan SolverStep, 33)},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Logf("Options for rack:\n%v", tt.fields.Rack)
			s := SolverStep{
				Mutex:  &sync.Mutex{},
				Rack:   tt.fields.Rack,
				tries:  tt.fields.tries,
				Parent: tt.fields.Parent,
			}
			s.wideMoveOptionsToChan(context.Background(), &tt.args.ch)
			close(tt.args.ch)

			i := 0
			for ss := range tt.args.ch {
				i++
				t.Logf("StepOption:\n%v", ss)
			}
			if i < len(tt.fields.Rack)-3 {
				t.Error("Should be more options here...")
			}
		})
	}
}

func TestSolve(t *testing.T) {
	type args struct {
		r Rack
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "Short test of main function",
			args: args{
				r: GenerateRack(3),
			},
		},
		// {
		// 	name: "Long test of main function",
		// 	args: args{
		// 		r: GenerateRack(9),
		// 	},
		// },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := findAllSolutions(tt.args.r)
			t.Logf("Found %d solutions", len(s))
			if len(s) == 0 {
				t.Error("Found no solution. Fail")
			}
		})
	}
}

func TestSolverStep_deepMoveOption(t *testing.T) {
	testRack := GenerateRack(12)

	testSolverStep := NewStartSolver(testRack)

	type fields struct {
		Mutex    *sync.Mutex
		Rack     Rack
		tries    *tries
		Root     *SolverStep
		Parent   *SolverStep
		Children []*SolverStep
	}
	type args struct {
		maxDepth int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    Rack
		want1   tryState
		wantErr bool
	}{
		{
			name: "solved",
			fields: fields{
				Mutex: &sync.Mutex{},
				Rack: Rack{
					Tube{content: [4]waterColour{1, 1, 1, 1}},
					Tube{content: [4]waterColour{2, 2, 2, 2}},
					Tube{content: [4]waterColour{3, 3, 3, 3}},
					Tube{content: [4]waterColour{0, 0, 0, 0}},
					Tube{content: [4]waterColour{0, 0, 0, 0}},
				},
				tries: &tries{
					Mutex:  &sync.Mutex{},
					states: map[rackSum]tryState{},
				},
				Root:     &testSolverStep,
				Parent:   nil,
				Children: []*SolverStep{},
			},
			args: args{
				maxDepth: 4,
			},
			want: Rack{
				Tube{content: [4]waterColour{1, 1, 1, 1}},
				Tube{content: [4]waterColour{2, 2, 2, 2}},
				Tube{content: [4]waterColour{3, 3, 3, 3}},
				Tube{content: [4]waterColour{0, 0, 0, 0}},
				Tube{content: [4]waterColour{0, 0, 0, 0}},
			},
			want1:   SUCCESS,
			wantErr: false,
		},
		{
			name: "almost solved",
			fields: fields{
				Mutex: &sync.Mutex{},
				Rack: Rack{
					Tube{content: [4]waterColour{1, 1, 1, 1}},
					Tube{content: [4]waterColour{2, 2, 2, 2}},
					Tube{content: [4]waterColour{0, 3, 3, 3}},
					Tube{content: [4]waterColour{0, 0, 0, 3}},
					Tube{content: [4]waterColour{0, 0, 0, 0}},
				},
				tries: &tries{
					Mutex:  &sync.Mutex{},
					states: map[rackSum]tryState{},
				},
				Root:     &testSolverStep,
				Parent:   nil,
				Children: []*SolverStep{},
			},
			args: args{
				maxDepth: 2,
			},
			want: Rack{
				Tube{content: [4]waterColour{1, 1, 1, 1}},
				Tube{content: [4]waterColour{2, 2, 2, 2}},
				Tube{content: [4]waterColour{3, 3, 3, 3}},
				Tube{content: [4]waterColour{0, 0, 0, 0}},
				Tube{content: [4]waterColour{0, 0, 0, 0}},
			},
			want1:   SUCCESS,
			wantErr: false,
		},
		{
			name: "unsolvable",
			fields: fields{
				Mutex: &sync.Mutex{},
				Rack: Rack{
					Tube{content: [4]waterColour{1, 1, 1, 1}},
					Tube{content: [4]waterColour{3, 2, 2, 2}},
					Tube{content: [4]waterColour{3, 3, 3, 2}},
				},
				tries: &tries{
					Mutex:  &sync.Mutex{},
					states: map[rackSum]tryState{},
				},
				Root:     &testSolverStep,
				Parent:   nil,
				Children: []*SolverStep{},
			},
			args: args{
				maxDepth: 2,
			},
			want: Rack{
				Tube{content: [4]waterColour{1, 1, 1, 1}},
				Tube{content: [4]waterColour{3, 2, 2, 2}},
				Tube{content: [4]waterColour{3, 3, 3, 2}},
			},
			want1:   DEADEND,
			wantErr: false,
		},
		{
			name: "unsolvable",
			fields: fields{
				Mutex: &sync.Mutex{},
				Rack: Rack{
					Tube{content: [4]waterColour{1, 2, 3, 4}},
					Tube{content: [4]waterColour{1, 2, 3, 4}},
					Tube{content: [4]waterColour{1, 2, 3, 4}},
					Tube{content: [4]waterColour{1, 2, 3, 4}},
					Tube{content: [4]waterColour{1, 2, 3, 4}},
					Tube{content: [4]waterColour{0, 0, 0, 0}},
				},
				tries: &tries{
					Mutex:  &sync.Mutex{},
					states: map[rackSum]tryState{},
				},
				Root:     &testSolverStep,
				Parent:   nil,
				Children: []*SolverStep{},
			},
			args: args{
				maxDepth: 20,
			},
			want: Rack{
				Tube{content: [4]waterColour{1, 2, 3, 4}},
				Tube{content: [4]waterColour{1, 2, 3, 4}},
				Tube{content: [4]waterColour{1, 2, 3, 4}},
				Tube{content: [4]waterColour{1, 2, 3, 4}},
				Tube{content: [4]waterColour{1, 2, 3, 4}},
				Tube{content: [4]waterColour{0, 0, 0, 0}},
			},
			want1:   DEADEND,
			wantErr: false,
		},
		{
			name: "unsolvable",
			fields: fields{
				Mutex: &sync.Mutex{},
				Rack: Rack{
					Tube{content: [4]waterColour{1, 2, 3, 4}},
					Tube{content: [4]waterColour{1, 2, 3, 4}},
					Tube{content: [4]waterColour{1, 2, 3, 4}},
					Tube{content: [4]waterColour{1, 2, 3, 4}},
					Tube{content: [4]waterColour{0, 0, 0, 0}},
					Tube{content: [4]waterColour{0, 0, 0, 0}},
				},
				tries: &tries{
					Mutex:  &sync.Mutex{},
					states: map[rackSum]tryState{},
				},
				Root:     &testSolverStep,
				Parent:   nil,
				Children: []*SolverStep{},
			},
			args: args{
				maxDepth: 200,
			},
			want: Rack{
				Tube{content: [4]waterColour{1, 1, 1, 1}},
				Tube{content: [4]waterColour{2, 2, 2, 2}},
				Tube{content: [4]waterColour{3, 3, 3, 3}},
				Tube{content: [4]waterColour{4, 4, 4, 4}},
				Tube{content: [4]waterColour{0, 0, 0, 0}},
				Tube{content: [4]waterColour{0, 0, 0, 0}},
			},
			want1:   SUCCESS,
			wantErr: false,
		},
		{
			name: "unknown",
			fields: fields{
				Mutex: &sync.Mutex{},
				Rack:  testRack,
				tries: &tries{
					Mutex:  &sync.Mutex{},
					states: map[rackSum]tryState{},
				},
				Root:     &testSolverStep,
				Parent:   nil,
				Children: []*SolverStep{},
			},
			args: args{
				maxDepth: 100,
			},
			want: Rack{
				Tube{content: [4]waterColour{1, 1, 1, 1}},
				Tube{content: [4]waterColour{2, 2, 2, 2}},
				Tube{content: [4]waterColour{3, 3, 3, 3}},
				Tube{content: [4]waterColour{4, 4, 4, 4}},
				Tube{content: [4]waterColour{5, 5, 5, 5}},
				Tube{content: [4]waterColour{6, 6, 6, 6}},
				Tube{content: [4]waterColour{7, 7, 7, 7}},
				Tube{content: [4]waterColour{8, 8, 8, 8}},
				Tube{content: [4]waterColour{9, 9, 9, 9}},
				Tube{content: [4]waterColour{10, 10, 10, 10}},
				Tube{content: [4]waterColour{11, 11, 11, 11}},
				Tube{content: [4]waterColour{12, 12, 12, 12}},
				Tube{content: [4]waterColour{0, 0, 0, 0}},
				Tube{content: [4]waterColour{0, 0, 0, 0}},
			},
			want1:   SUCCESS,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &SolverStep{
				Mutex:    tt.fields.Mutex,
				Rack:     tt.fields.Rack,
				tries:    tt.fields.tries,
				Root:     tt.fields.Root,
				Parent:   tt.fields.Parent,
				Children: tt.fields.Children,
			}
			got, got1, err := s.deepMoveOption(tt.args.maxDepth)
			if (err != nil) != tt.wantErr {
				t.Errorf("SolverStep.deepMoveOption() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got.Rack.CheckSum() != tt.want.CheckSum() {
				t.Errorf("SolverStep.deepMoveOption() got = %s\n%v\n, want %s\n%v\n", got.Rack.CheckSum(), got, tt.want.CheckSum(), tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("SolverStep.deepMoveOption() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
