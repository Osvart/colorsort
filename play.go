package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func clearScr(msg ...interface{}) {
	cmd := exec.Command("clear") //Linux example, its tested
	cmd.Stdout = os.Stdout
	cmd.Run()
	fmt.Println("Water Sort")
	fmt.Print("(h for help)\n\n")

	for _, m := range msg {
		fmt.Println(m)
	}
}

func Play(r Rack) {
	clearScr()
	reader := bufio.NewReader(os.Stdin)

	step := newGame(fSize)

	var solution *SolverStep

	for {
		printRack(step.Rack)
		fmt.Print("Enter command or move $> ")
		cmdLn, _ := reader.ReadString('\n')
		cmdLn = strings.ToLower(
			strings.TrimSpace(
				strings.Replace(cmdLn, "\n", "", -1),
			),
		)
		cmd := strings.Split(cmdLn, " ")

		switch cmd[0] {
		case "u":
			step = step.undo()
		case "h", "":
			clearScr(helpText())
		case "o":
			clearScr(moveOptions(step.Rack))
		case "s":
			st := time.Now()
			solution = Solve(step.Rack)
			fmt.Printf("spent %v, found a solution\n", time.Since(st))
			if solution != nil {
				fmt.Printf("%s", solution)
			}
		case "r":
			n, err := strconv.Atoi(cmd[1])
			if err != nil {
				clearScr("Failed to parse size, format must be > r NN", err)
				continue
			}
			fSize = n
			if step.Parent == nil {
				step = newGame(fSize)
			}
			clearScr(fmt.Sprintf("Number of tubes set to %d", fSize), "Will be effecitve on next new rack if you were allready in progress")
		case "n":
			step = newGame(fSize)
			clearScr()
		case "q":
			fmt.Println("Quit! Bye!")
			return
			//"Hidden" shortcuts for debuging and such:
		case "save":
			err := saveStep(step, cmd[1:])
			if err != nil {
				clearScr("Failed to write to file", err)
				continue
			}
			clearScr("Saved to file", cmd[1])
		case "j":
			j, err := json.Marshal(step)
			if err != nil {
				clearScr("Failed to marshall json", err)
			}
			clearScr(string(j))
		case "+":
			fSize++
			step = newGame(fSize)
			clearScr()
		case "-":
			fSize--
			step = newGame(fSize)
			clearScr()

		default:
			from, to, err := parseMoves(cmdLn)
			if err != nil {
				clearScr(fmt.Sprintf("Unknown command, or move: %s, \nformat for move is \"from to\" (<int> <int>)\n (%s)", cmdLn, err), helpText())
				continue
			}
			nr, err := step.Move(from, to)
			log.Println(nr)
			if err != nil {
				fmt.Printf("Illegal move from %d to %d, %v", from, to, err)
			} else {
				step = step.childStep(nr)
				clearScr()
			}
			if step.Done() {
				fmt.Println(`CONGRATULATIONS! You're the best \o/`)
				continue
			}

		}

	}
}

func newGame(s int) *SolverStep {
	r := GenerateRack(s)

	st := NewStartSolver(r)
	return &st
}

func saveStep(s *SolverStep, params []string) error {
	ss := make([]*SolverStep, 0)
	for st := s; st != nil; st = st.Parent {
		ss = append(ss, st)
	}
	j, err := json.Marshal(ss)
	if err != nil {
		panic("Failed json" + err.Error())
	}

	return os.WriteFile(params[0], j, 0644)
}

func (s *SolverStep) undo() *SolverStep {
	if s.Parent == nil {
		fmt.Println("Nothing to undo...")
		return s
	}
	return s.Parent
}

func parseMoves(c string) (from int, to int, err error) {
	numsRx := regexp.MustCompile(`\d+`)

	// m := strings.Split(c, " ")
	m := numsRx.FindAllString(c, 2)
	if len(m) != 2 {
		return 0, 0, fmt.Errorf("wrong number of arguments, need exactly two, from and to")
	}

	from, err = strconv.Atoi(m[0])
	if err != nil {
		return 0, 0, err
	}
	to, err = strconv.Atoi(m[1])
	if err != nil {
		return 0, 0, err
	}
	return from, to, nil
}

func helpText() string {
	s := "Usage:\n"
	s += "\th \t - help (this message)\n"
	s += "\t \t - To make a move, enter <from> and <to> like this: 3 5\n"
	s += "\tu \t - Undo last move\n"
	s += "\to \t - Show possible moves\n"
	s += "\ts \t - Let the computer solve from here\n"
	s += "\tn \t - Get a new rack\n"
	s += "\tr NN\t - Resize number of tubes in rack to NN (effective at next new)\n"
	s += "\tq \t - Quit!\n"
	return s
}

func moveOptions(r Rack) string {
	m := "Possible moves:\n"

	for _, o := range r.legalMoves() {
		m += o.TubeLabels()
		m += o.String()
	}

	return m
}

func printRack(r Rack) {
	fmt.Print(r.TubeLabels())
	fmt.Println(r)
}

//TODO: move this to tubes.go
func (r Rack) TubeLabels() string {
	s := "\n"
	d := ""
	for i := range r {
		s += fmt.Sprintf("%2d%2s", i, "")
		d += "--  "
	}
	return s + "\n" + d + "\n"
}
