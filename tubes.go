package main

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"math/rand"
	"sort"
	"time"

	"github.com/fatih/color"
)

type Rack []Tube

func GenerateRack(size ...int) (r Rack) {
	rand.Seed(time.Now().UnixNano())
	tubeCount := rand.Intn(8) + 4
	if len(size) > 0 && size[0] > 0 {
		tubeCount = size[0]
	}

	a := make([]waterColour, tubeCount*4)
	for i := 0; i < tubeCount*4; i++ {
		n := i%tubeCount + 1
		a[i] = waterColour(n)
	}
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(a), func(i, j int) { a[i], a[j] = a[j], a[i] })
	// log.Println(a)

	for i := 0; i < tubeCount; i++ {
		var t Tube
		copy(t.content[:], a[i*4:i*4+4])
		r = append(r, t)
	}
	r = append(r, Tube{}, Tube{})
	// if tubeCount > 11 {
	// 	r = append(r, Tube{})
	// }
	return r
}

func (r Rack) String() string {
	var s string
	for i := 0; i < 4; i++ {
		for j := 0; j < len(r); j++ {
			s += fmt.Sprintf("%s%2s", r[j].content[i], "")
		}
		s += "\n"
	}
	return s
}

func (r Rack) Done() bool {
	if len(r) == 0 {
		return false
	}
	for _, t := range r {
		if !(t.Done() || t.Empty()) {
			return false
		}
	}
	return true
}

func (r Rack) Move(from int, to int) (Rack, error) {
	if from >= len(r) || to >= len(r) {
		return nil, fmt.Errorf("cannot move from %d to %d. out of range", from, to)
	}
	ret := append(Rack{}, r...) //Doing this to copy content, not pointer

	fcol, fcount := ret[from].Top()
	tcol, _ := ret[to].Top()

	if ret[from].Empty() {
		return nil, fmt.Errorf("illegal move, empty from-tube")
	}
	if fcol != tcol && !ret[to].Empty() {
		return nil, fmt.Errorf("illegal move, wrong colour")
	}

	moved := 0
	for j := 3; j >= 0 && moved < fcount; j-- {
		if ret[to].content[j] != 0 {
			continue
		}
		ret[to].content[j] = fcol
		for i := 0; i < 4; i++ {
			if ret[from].content[i] == 0 {
				continue
			}
			ret[from].content[i] = 0
			break
		}
		moved++
	}

	return ret, nil
}

func (r Rack) Sort() Rack {
	ret := append(Rack{}, r...)
	sort.Slice(ret, func(i, j int) bool { return ret[i].SortValue() < ret[j].SortValue() })
	return ret
}

type rackSum [16]byte

func (r rackSum) String() string {
	return hex.EncodeToString(r[:])
}
func (r Rack) CheckSum() rackSum {
	return md5.Sum([]byte(r.Sort().String()))
}

//Tube

type waterColour int

func (w waterColour) String() string {
	bg := color.BgBlack
	if w == 0 {
		bg = 0
	} else if w < 8 {
		bg = color.BgBlack + color.Attribute(w)
	} else {
		bg = color.BgHiBlack + color.Attribute(w-6)
	}

	fg := color.FgHiWhite
	if bg >= 102 || bg == 47 {
		fg = color.FgBlack
	}

	ct := color.New(bg, fg, color.Bold).SprintfFunc()("%2d", w)

	return ct
}

type Tube struct {
	content [4]waterColour
}

func (t Tube) MarshalJSON() ([]byte, error) {
	s := make([]int, 0)
	for _, v := range t.content {
		s = append(s, int(v))
	}
	return json.Marshal(s)
}

func (t Tube) Top() (colour waterColour, count int) {
	for _, c := range t.content {
		if c == 0 {
			continue
		}
		if colour == 0 {
			colour = c
			count = 1
		} else if c == colour {
			count++
		} else {
			break
		}
	}
	return colour, count
}

func (t *Tube) Done() bool {
	var l waterColour
	for _, c := range t.content {
		if c == 0 {
			return false
		} else if l == 0 {
			l = c
		} else if c != l {
			return false
		}
	}
	return true
}

func (t *Tube) Empty() bool {
	return t.content[3] == 0
}

func (t *Tube) CanReceive(col waterColour) bool {
	if t.content[0] != 0 {
		return false
	}

	top, _ := t.Top()
	return col == top || t.Empty()
}

func (t Tube) SortValue() int {
	v := 0
	for i, c := range t.content {
		v += int(c) * (i + 1) * 16
	}
	return v
}
