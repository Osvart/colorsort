package main

import (
	"flag"
	"fmt"
)

var (
	fSize  int
	fPlay  bool
	fSolve bool
)

func main() {

	flag.IntVar(&fSize, "size", 0, "number of filled tubes in rack")
	flag.BoolVar(&fSolve, "solve", false, "let computer solve")
	flag.BoolVar(&fPlay, "play", false, "play the game")
	flag.Parse()

	var r Rack
	if fSize > 0 {
		r = GenerateRack(fSize)
	} else {
		r = GenerateRack()
	}

	if fPlay {
		Play(r)
	}

	if fSolve {
		s := findAllSolutions(r)
		fmt.Printf("Found %d solution(s)\n", len(s))
		if len(s) > 0 {
			fmt.Println("Showing one")
			sol := ""
			for st := &s[0]; st != nil; st = st.Parent {
				sol += fmt.Sprintf("State: %s, %v, \n%v\n%v\n",
					st.Rack.CheckSum(),
					st.tries.GetState(st.Rack.CheckSum()),
					// st.ListChildren(),
					"",
					st.Rack,
				)
			}
			fmt.Println(sol)

		}

	}

}
