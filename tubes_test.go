package main

import (
	"reflect"
	"testing"
)

func TestTube_Top(t *testing.T) {
	type fields struct {
		content [4]waterColour
	}
	tests := []struct {
		name       string
		fields     fields
		wantColour waterColour
		wantCount  int
	}{
		// TODO: Add test cases.
		{
			name: "empty",
			fields: fields{
				content: [4]waterColour{0, 0, 0, 0},
			},
			wantColour: 0,
			wantCount:  0,
		},
		{
			name: "oneTwo",
			fields: fields{
				content: [4]waterColour{0, 1, 1, 2},
			},
			wantColour: 1,
			wantCount:  2,
		},
		{
			name: "threeOne",
			fields: fields{
				content: [4]waterColour{3, 4, 3, 1},
			},
			wantColour: 3,
			wantCount:  1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := Tube{
				content: tt.fields.content,
			}
			gotColour, gotCount := tr.Top()
			if gotColour != tt.wantColour {
				t.Errorf("Tube.Top() gotColour = %v, want %v", gotColour, tt.wantColour)
			}
			if gotCount != tt.wantCount {
				t.Errorf("Tube.Top() gotCount = %v, want %v", gotCount, tt.wantCount)
			}
		})
	}
}

func TestTube_Done(t *testing.T) {
	type fields struct {
		content [4]waterColour
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "empty",
			fields: fields{
				content: [4]waterColour{0, 0, 0, 0},
			},
			want: false,
		},
		{
			name: "oneOne",
			fields: fields{
				content: [4]waterColour{0, 1, 1, 1},
			},
			want: false,
		},
		{
			name: "oneTwo",
			fields: fields{
				content: [4]waterColour{0, 1, 1, 2},
			},
			want: false,
		},
		{
			name: "threeOne",
			fields: fields{
				content: [4]waterColour{3, 4, 3, 3},
			},
			want: false,
		},
		{
			name: "threes",
			fields: fields{
				content: [4]waterColour{3, 3, 3, 3},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &Tube{
				content: tt.fields.content,
			}
			if got := tr.Done(); got != tt.want {
				t.Errorf("Tube.Done() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTube_CanReceive(t *testing.T) {
	type fields struct {
		content [4]waterColour
	}
	type args struct {
		col waterColour
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "full",
			fields: fields{
				content: [4]waterColour{2, 3, 4, 5},
			},
			args: args{
				col: 2,
			},
			want: false,
		},
		{
			name: "empty",
			fields: fields{
				content: [4]waterColour{0, 0, 0, 0},
			},
			args: args{
				col: 2,
			},
			want: true,
		},
		{
			name: "canReceive",
			fields: fields{
				content: [4]waterColour{0, 3, 4, 5},
			},
			args: args{
				col: 3,
			},
			want: true,
		},
		{
			name: "canReceive2",
			fields: fields{
				content: [4]waterColour{0, 0, 0, 5},
			},
			args: args{
				col: 5,
			},
			want: true,
		},
		{
			name: "wrong color",
			fields: fields{
				content: [4]waterColour{0, 3, 3, 5},
			},
			args: args{
				col: 5,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &Tube{
				content: tt.fields.content,
			}
			if got := tr.CanReceive(tt.args.col); got != tt.want {
				t.Errorf("Tube.CanReceive() = %v, want %v", got, tt.want)
				t.Logf("trying to fit %v into tube %v", tt.args.col, tt.fields.content)
			}
		})
	}
}

func TestRack_Move(t *testing.T) {
	type args struct {
		from int
		to   int
	}

	testRack := Rack{
		Tube{content: [4]waterColour{1, 2, 3, 4}},
		Tube{content: [4]waterColour{2, 2, 3, 4}},
		Tube{content: [4]waterColour{1, 2, 3, 4}},
		Tube{content: [4]waterColour{0, 0, 2, 4}},
		Tube{content: [4]waterColour{0, 2, 3, 4}},
		Tube{content: [4]waterColour{0, 0, 0, 0}},
		Tube{content: [4]waterColour{0, 0, 0, 0}},
	}

	generatedRack := GenerateRack()
	t.Logf("Generated rack\n%v", generatedRack)

	tests := []struct {
		name    string
		r       Rack
		args    args
		want    Rack
		wantErr bool
	}{
		{
			name:    "illegal empty",
			r:       generatedRack,
			args:    args{from: len(generatedRack) - 1, to: 0},
			want:    nil,
			wantErr: true,
		},
		{
			name:    "illegal wrong",
			r:       testRack,
			args:    args{from: 0, to: 3},
			want:    nil,
			wantErr: true,
		},
		{
			name: "move two",
			r:    testRack,
			args: args{from: 1, to: 3},
			want: Rack{
				Tube{content: [4]waterColour{1, 2, 3, 4}},
				Tube{content: [4]waterColour{0, 0, 3, 4}},
				Tube{content: [4]waterColour{1, 2, 3, 4}},
				Tube{content: [4]waterColour{2, 2, 2, 4}},
				Tube{content: [4]waterColour{0, 2, 3, 4}},
				Tube{content: [4]waterColour{0, 0, 0, 0}},
				Tube{content: [4]waterColour{0, 0, 0, 0}},
			},
			wantErr: false,
		},
		{
			name: "move one",
			r:    testRack,
			args: args{from: 1, to: 4},
			want: Rack{
				Tube{content: [4]waterColour{1, 2, 3, 4}},
				Tube{content: [4]waterColour{0, 2, 3, 4}},
				Tube{content: [4]waterColour{1, 2, 3, 4}},
				Tube{content: [4]waterColour{0, 0, 2, 4}},
				Tube{content: [4]waterColour{2, 2, 3, 4}},
				Tube{content: [4]waterColour{0, 0, 0, 0}},
				Tube{content: [4]waterColour{0, 0, 0, 0}},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// t.Logf("before, testRack looks like this:\n%v\n, and tt.r:\n%v", testRack, tt.r)
			got, err := tt.r.Move(tt.args.from, tt.args.to)
			if (err != nil) != tt.wantErr {
				t.Errorf("Rack.Move() error = \n%v\n, wantErr \n%v\n", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Rack.Move() = \n%v\n, want \n%v\n", got, tt.want)
				t.Logf("testRack looks like this:\n%v\n, and tt.r:\n%v", testRack, tt.r)
				if err != nil {
					t.Log("Error:", err)
				}
			}
		})
	}
}

func TestTube_SortValue(t *testing.T) {
	type fields struct {
		content [4]waterColour
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{
			name: "1",
			fields: fields{
				content: [4]waterColour{0, 0, 0, 0},
			},
			want: 0,
		},
		{
			name: "1",
			fields: fields{
				content: [4]waterColour{2, 4, 3, 1},
			},
			want: 368,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := Tube{
				content: tt.fields.content,
			}
			if got := tr.SortValue(); got != tt.want {
				t.Errorf("Tube.SortValue() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRack_Sort(t *testing.T) {

	testRack := Rack{
		Tube{content: [4]waterColour{1, 2, 3, 4}},
		Tube{content: [4]waterColour{2, 2, 3, 4}},
		Tube{content: [4]waterColour{1, 2, 3, 4}},
		Tube{content: [4]waterColour{0, 0, 2, 4}},
		Tube{content: [4]waterColour{0, 2, 3, 4}},
		Tube{content: [4]waterColour{0, 0, 0, 0}},
		Tube{content: [4]waterColour{0, 0, 0, 0}},
	}

	genRack := GenerateRack(4)
	sortRack := genRack.Sort()

	tests := []struct {
		name string
		r    Rack
		want Rack
	}{
		{
			name: "fixedRack",
			r:    testRack,
			want: Rack{
				Tube{content: [4]waterColour{0, 0, 0, 0}},
				Tube{content: [4]waterColour{0, 0, 0, 0}},
				Tube{content: [4]waterColour{0, 0, 2, 4}},
				Tube{content: [4]waterColour{0, 2, 3, 4}},
				Tube{content: [4]waterColour{1, 2, 3, 4}},
				Tube{content: [4]waterColour{1, 2, 3, 4}},
				Tube{content: [4]waterColour{2, 2, 3, 4}},
			},
		},
		{
			name: "generatedRack sort twice",
			r:    genRack,
			want: sortRack,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.r.Sort(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Rack.Sort() = \n%v\n, want \n%v", got, tt.want)
			}
		})
	}
}
