package main

// This is here for develop and debug only. Please safely ignore
import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/fatih/color"

	"testing"
)

func TestColors(t *testing.T) {
	t.Skip()
	color.Cyan("Prints text in cyan.")
}

func TestJsonRack(t *testing.T) {
	// t.Skip()
	testRack := Rack{
		Tube{content: [4]waterColour{1, 2, 3, 4}},
		Tube{content: [4]waterColour{2, 2, 3, 4}},
		Tube{content: [4]waterColour{1, 2, 3, 4}},
		Tube{content: [4]waterColour{0, 0, 2, 4}},
		Tube{content: [4]waterColour{0, 2, 3, 4}},
		Tube{content: [4]waterColour{0, 0, 0, 0}},
		Tube{content: [4]waterColour{0, 0, 0, 0}},
	}

	j, _ := json.Marshal(NewStartSolver(testRack))

	fmt.Printf("Json: %s\n", j)

	now := time.Now()
	j, _ = json.Marshal(now)

	fmt.Printf("Jsontimestamp: %s\n", j)
	fmt.Println(now.Format(time.Kitchen))

}
