package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"
	"time"
)

var ch chan SolverStep

const CHANLENGTH = 99999

type SolverStep struct {
	*sync.Mutex
	Rack `json:"rack"`
	*tries
	Root     *SolverStep
	Parent   *SolverStep
	Children []*SolverStep `json:"-"`
}

func (s SolverStep) MarshalJSON() ([]byte, error) {
	c := make([]string, 0)
	for _, v := range s.Children {
		c = append(c, v.CheckSum().String())
	}
	r := struct {
		ID       string      `json:"id"`
		Rack     Rack        `json:"rack"`
		Parent   interface{} `json:"parent"`
		Root     string      `json:"root"`
		Children []string    `json:"children"`
	}{
		ID:       s.CheckSum().String(),
		Rack:     s.Rack,
		Root:     s.Root.CheckSum().String(),
		Children: c,
	}
	if s.Parent != nil {
		r.Parent = s.Parent.CheckSum().String()
	}

	return json.Marshal(r)
}

type tries struct {
	*sync.Mutex
	states map[rackSum]tryState
}

type tryState int

const (
	UNKNOWN  tryState = iota
	OPEN     tryState = iota
	SEARCHED tryState = iota
	DEADEND  tryState = iota
	SOLVABLE tryState = iota
	SUCCESS  tryState = iota
)

func NewStartSolver(r Rack) SolverStep {
	s := SolverStep{
		Mutex:    &sync.Mutex{},
		Rack:     r,
		tries:    &tries{Mutex: &sync.Mutex{}, states: map[rackSum]tryState{}},
		Parent:   nil,
		Children: []*SolverStep{},
	}
	s.Root = &s
	return s
}

func (t *tries) Open(r rackSum) {
	t.Lock()
	defer t.Unlock()
	t.states[r] = OPEN
}
func (t *tries) DeadEnd(r rackSum) {
	t.Lock()
	defer t.Unlock()
	t.states[r] = DEADEND
}
func (t *tries) Success(r rackSum) {
	t.Lock()
	defer t.Unlock()
	t.states[r] = SUCCESS
}
func (t *tries) Solvable(r rackSum) {
	t.Lock()
	defer t.Unlock()
	t.states[r] = SOLVABLE
}
func (t *tries) Set(r rackSum, s tryState) {
	t.Lock()
	defer t.Unlock()
	t.states[r] = s
}
func (t *tries) GetState(r rackSum) tryState {
	t.Lock()
	defer t.Unlock()
	s, ok := t.states[r]
	if !ok {
		return UNKNOWN
	}
	return s
}

func Solve(r Rack) *SolverStep {
	s := NewStartSolver(r)

	res, state, _ := s.deepMoveOption(len(r) * 10)
	if state != SUCCESS {
		return nil
	}
	return &res
}

func findAllSolutions(r Rack) []SolverStep {
	workers := len(r)

	s := NewStartSolver(r)
	s.tries.Open(r.CheckSum())

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	solCh := make(chan SolverStep, workers)
	solutions := make([]SolverStep, 0)

	ch = make(chan SolverStep, CHANLENGTH)
	ch <- s

	wg := sync.WaitGroup{}
	for i := 0; i < workers; i++ {
		wg.Add(1)
		go func(i int) {
			handleSolutions(ctx, &ch, &solCh, i)
			wg.Done()
		}(i)
	}

	wg.Wait()
	close(ch)
	close(solCh)

	for sol := range solCh {
		solutions = append(solutions, sol)
	}

	// fmt.Printf("DEBUG: Found %d solutions. Done solve()\n", len(solutions))
	return solutions
}

func handleSolutions(ctx context.Context, stepChan *chan SolverStep, solvChan *chan SolverStep, wid ...int) {
	if len(*stepChan) == 0 {
		time.Sleep(time.Millisecond * 100)
	}

	for len(*stepChan) > 0 {
		select {
		case <-ctx.Done():
			if len(wid) > 0 {
				fmt.Println("DEBUG: Context cancelled worker", wid[0])
			}
			return
		case s := <-*stepChan:
			if fPlay {
				fmt.Fprintf(os.Stderr, " %7d in chan, %8d processed\r", len(*stepChan), len(s.tries.states))
			}
			//TODO: Store this somewhere

			if s.Done() {
				s.tries.Success(s.CheckSum())

				for st := &s; st != nil; st = st.Parent {
					st.Set(st.CheckSum(), SOLVABLE)
				}
				if len(wid) > 0 {
					fmt.Println("DEBUG: Sending solution to chan from worker", wid[0])
				}
				*solvChan <- s
				// return &ss
			}

			maxDepth := 12000
			for _, err := s.wideMoveOptionsToChan(ctx, stepChan); err != nil; _, err = s.wideMoveOptionsToChan(ctx, stepChan) {
				option, state, err := s.deepMoveOption(maxDepth)
				// fmt.Printf("DEBUG: sent %s to deepsearch, returned %s, %d, %s", s.CheckSum(), option.CheckSum(), state, err.Error())
				// os.Exit(1)
				s.Set(option.CheckSum(), state)
				if err != nil {
					log.Println("finding deep options failed. could not clear chan")
					option, _ = deepMoveOptionsFromChan(ctx, stepChan, maxDepth)
				}
				select {
				case *stepChan <- option:
				default:
					continue
				}
			}

		case <-time.After(time.Millisecond * 250):
			continue //retest chan-len
		}
	}

	if len(wid) > 0 {
		fmt.Println("DEBUG: Returning because channel empty", wid[0])
	}

	// return nil
}

func (s *SolverStep) deepMoveOption(maxDepth int) (SolverStep, tryState, error) {
	var (
		option SolverStep
		state  tryState
		err    error
	)
	if s.Done() {
		return *s, SUCCESS, nil
	}
	if maxDepth == 0 {
		return *s, UNKNOWN, fmt.Errorf("max Depth reached")
	}

	// fmt.Printf("DEBUG: maxdepth=%d running test for %s, state: %d, \n%s\n", maxDepth, s.CheckSum(), s.GetState(s.CheckSum()), s.Rack)

	mv := s.legalMoves()

	if len(mv) == 0 {
		return *s, DEADEND, nil
	}

	optCount := 0
	for _, r := range mv {
		st := s.childStep(r)
		// fmt.Printf("DEBUG: maxdepth=%d running test for child %s, state: %d, \n%s\n", maxDepth, st.CheckSum(), st.GetState(s.CheckSum()), st.Rack)
		if st.GetState(st.CheckSum()) != UNKNOWN {
			continue
		}
		s.Set(r.CheckSum(), SEARCHED)

		option, state, err = st.deepMoveOption(maxDepth - 1)
		s.Set(option.CheckSum(), state)
		if err != nil || state == SUCCESS {
			return option, state, err
		}

	}

	if optCount == 0 {
		return *s, DEADEND, nil
	}

	s.Open(s.CheckSum())
	return *s, SEARCHED, nil
}

func deepMoveOptionsFromChan(ctx context.Context, stepChan *chan SolverStep, maxDepth int) (SolverStep, error) {
	s := <-*stepChan
	option, state, err := s.deepMoveOption(maxDepth)
	s.Set(option.CheckSum(), state)
	return option, err

}

func (s SolverStep) wideMoveOptionsToChan(ctx context.Context, ch *chan SolverStep) (int, error) {
	r := append(Rack{}, s.Rack...)

	optCount := 0

	m := r.legalMoves()
	if len(m) == 0 {
		s.tries.DeadEnd(s.CheckSum())
	}

	for _, r := range m {
		if s.tries.GetState(r.CheckSum()) != UNKNOWN {
			continue
		}
		newStep := s.childStep(r)
		optCount++

		select {
		case *ch <- *newStep:
			s.tries.Open(newStep.CheckSum())
			continue
		default:
			return optCount, fmt.Errorf("channel buffer full. %d records ", len(*ch))
		}
	}

	return optCount, nil
}

func (r Rack) legalMoves() []Rack {
	ret := make([]Rack, 0)

	for i, t := range r {
		colour, _ := t.Top()
		if colour == 0 {
			continue
		}
		if t.Done() {
			continue
		}
		for j, t2 := range r {
			if i == j {
				continue
			}
			if t2.CanReceive(colour) {
				stepRack, err := r.Move(i, j)
				if err != nil {
					log.Fatalln("Move failed, should not be possible", err)
				}
				ret = append(ret, stepRack)
			}
		}
	}

	return ret
}

func (s *SolverStep) childStep(r Rack) *SolverStep {
	newStep := SolverStep{
		Mutex:    s.Mutex,
		Rack:     r,
		tries:    s.tries,
		Root:     s.Root,
		Parent:   s,
		Children: []*SolverStep{},
	}

	s.Lock()
	defer s.Unlock()
	if s.Children == nil || len(s.Children) == 0 {
		s.Children = []*SolverStep{&newStep}
	} else {
		s.Children = append(s.Children, &newStep)

	}
	return &newStep
}

func (s *SolverStep) ListChildren() []rackSum {
	s.Lock()
	defer s.Unlock()
	if len(s.Children) == 0 {
		return nil
	}

	ret := make([]rackSum, 0)
	for _, ch := range s.Children {
		ret = append(ret, ch.CheckSum())
	}

	return ret
}
